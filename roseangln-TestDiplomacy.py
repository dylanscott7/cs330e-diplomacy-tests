from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve 

class TestDiplomacy (TestCase):

    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i,  [['A', 'Madrid', 'Hold']])

    def test_read_2(self):
        s = ""
        i = diplomacy_read(s)
        self.assertEqual(i,  [])

    # -----
    # eval
    # -----

    def test_eval_1(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"]])
        self.assertEqual(v, [["A", "Madrid"]])

    def test_eval_2(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"],["B", "Barcelona", "Move", "Madrid"]])
        self.assertEqual(v, [["A", "[dead]"], ["B", "[dead]"]])

    def test_eval_3(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"],["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"]])
        self.assertEqual(v, [["A", "[dead]"],["B", "Madrid"], ["C", "London"]])

    def test_eval_4(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"],["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Move", "Madrid"]])
        self.assertEqual(v, [["A", "[dead]"],["B", "[dead]"], ["C", "[dead]"]])

    def test_eval_5(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"],["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"], ["D", "Austin", "Move", "London"]])
        self.assertEqual(v, [["A", "[dead]"],["B", "[dead]"], ["C", "[dead]"], ["D", "[dead]"]])

    def test_eval_6(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "A"]])
        self.assertEqual(v, [["A", "Madrid"],["B", "[dead]"], ["C", "London"]])

    def test_eval_7(self):
        v = diplomacy_eval([["A", "Madrid", "Support", "B"],["B", "Barcelona", "Madrid"]])
        self.assertEqual(v, [["A", "Madrid"], ["B", "Barcelona"]])

    def test_eval_8(self):
        v = diplomacy_eval([["A", "Madrid", "Move", "Barcelona"],["B", "Barcelona", "Move", "Madrid"]])
        self.assertEqual(v, [["A", "[dead]"], ["B", "[dead]"]])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [["A", "Madrid", "Hold"]])
        self.assertEqual(w.getvalue(), "A Madrid\n")

    # ----
    # solve
    # ---- 

    def test_solve_1(self):
        # a test case in the project description
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        # testing the cyclic movement of the armies
        r = StringIO("A Madrid Support B\nB Barcelona Support D\nC London Move Barcelona\nD Austin Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Austin\n")

    def test_solve_3(self):
        # a test case in the project description
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Austin Support B\nE Houston Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\nD Austin\nE Houston\n")

    def test_solve_4(self):
        # a test case in the project description
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC London\n")

    def test_solve_5(self):
        # testing hold of all armies
        r = StringIO("A Madrid Hold\nB Barcelona Hold\nC London Hold\nD Austin Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD Austin\n")


if __name__ == "__main__":
    main()

""" #pragma: no cover
................
----------------------------------------------------------------------
Ran 16 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          71      0     44      0   100%
TestDiplomacy.py      67      0      0      0   100%
--------------------------------------------------------------
TOTAL                138      0     44      0   100%
"""

